import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookingService } from '../booking.service';
import { ToastrService } from 'ngx-toastr';
import { CurrencyPipe } from '@angular/common';
import { error } from 'jquery';
import { computeMsgId } from '@angular/compiler';


declare var Razorpay:any;
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  [x: string]: any;
  services: any;
 
  servToBook: any;
  custId:any;
  cartData:any;
  filterproducts:any;
    allProducts:any;
    searchInput="";



  bookServe={
   
    serviceType:'',
    servicePrice:'',
    date:'',
    adress:'',
    number:''
  
  }

  constructor(private service: BookingService, private router: Router, private http: HttpClient, private toastr: ToastrService) {}

  ngOnInit(): void {
    this.getServices();
    this. custId= localStorage.getItem('id');
    
    
  }

  getServices() {
    this.service.getServices().subscribe(
      (data: any) => {
        this.services = data;
        this.filterproducts=data;
        this.allProducts=data;
      },
      (error: any) => {
        console.error('Error loading services:', error);
      }
    );
  }

  book(serv: any) {
    this.bookServe = { ...serv };
  }

  bookservices(custId:any){
    const RazorpayOptions={
      currency:'INR',
      amount:this.bookServe.servicePrice,
      key:'rzp_test_A475cK1t4w7tcw',

      theme:{
        color:'#0c238a'
      },
      modal:{
        ondismiss:()=>{
          this.toastr.warning("payment Cancelled");
          this.service.getBookingBycustId(custId).subscribe((data:any)=>{
            this.toastr.success("Booking Successfull");
          })
        }
      }
    }

    const successCallback=(paymentid:any)=>{
      this.toastr.success('Payment Successfull');
     this.registerBooking(this.custId);
    
     
    }

    const errorCallback=(error:any)=>{
      this.toastr.error('Payment Failed');
    }

    Razorpay.open(RazorpayOptions,successCallback,errorCallback)

    
    
  }


  registerBooking(custId:any){
    const url =`http://localhost:8080/regBookbyCustId/${custId}`;
    this.http.post(url,this.bookServe).subscribe((response)=>{
      this.toastr.success("Booking Successfull")
    },
  (error)=>{
    this.toastr.error("Booking Failed")
  })
    }
    
  
  
  


  addToCart(serv:any){

 this.cartData={
  cName:serv.companyName,
  image:serv.imageUrl,
  descr:serv.description,
  serName:serv.serviceType,
  number:serv.mobileNumber,
  techName:serv.technicianName,
  price:serv.servicePrice,
  customer:{
    custId:this.custId,
  }

 }
 this.service.cartItems(this.cartData,this.custId).subscribe((result:any)=>{
  this.toastr.success("Service Added to Cart SuccesFully");
  console.log(result);
  
 })
  }
  filterProducts() {
    this.filterproducts = this.allProducts.filter(
      (service: any) => {
        return (
          service.companyName.toLowerCase().includes(this.searchInput.toLowerCase()) ||
          service.description.toLowerCase().includes(this.searchInput.toLowerCase()) ||
          service.serviceType.toLowerCase().includes(this.searchInput.toLowerCase())
        );
      }
    );
    console.log('Filtered products:', this.filterproducts); 
  }
  

 


 
}


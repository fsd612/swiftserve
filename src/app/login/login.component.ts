import { Component, OnInit } from '@angular/core';
import { BookingService } from '../booking.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  Users: any;
  Company: any;
  Admin: any;
  email: any;
  password: any;
  id:any;
  adminname:any;
name:any;
firstName:any;
  constructor(private service: BookingService, private router: Router, private toastr: ToastrService,private local:LocalStorageService) { }

  ngOnInit(): void {
    
   

    
  }

  loginsubmit(loginform: any) {
    if (loginform.valid) 
      {
      const email = loginform.value.email;
      const password = loginform.value.password;
    
  
      this.service.validateAdmin(email, password).subscribe((adminResponse: any) => {
        if (adminResponse === "true")
           {
          this.service.setAdminLoggedIn();
          this.local.store('adminname',this.adminname);
          console.log(this.adminname);
          
          this.toastr.success("Admin Logged In Successfully");
          this.router.navigate(['/home']);
          this.local.store('status',true);
          return;
        }
         else 
         {
          this.service.validate(email, password).subscribe((companyResponse: any) => {
            const emailJson = JSON.stringify(email);
            this.local.store('email', emailJson);
            console.log(email);
            
            console.log(companyResponse);
            let {id,status,name}=companyResponse
            console.log(id,status,name);
            if (status ) 
              {
              
              
              this.service.validateCompanyStatus(email).subscribe((status: any) => {
                if (status === "true")
                   {
                  console.log(this.Company);
                  localStorage.setItem('id',companyResponse.id);
                  console.log(id);
                  const nameJson = JSON.stringify(name);
                  this.local.store('name', nameJson);
                   console.log(name);

                const emailJson = this.local.retrieve('email');
                console.log(emailJson);
  
                  
                  
                    this.service.setCompanyLoggedIn();
                    this.local.store('name',this.name);
                    this.toastr.success("Company Logged In Successfully");
                    this.router.navigate(['/home']);
                    this.local.store('cstatus',true);
                } else
                 {
                  this.toastr.error("Admin has not approved your request yet");
                }
              });
            } 
            else {
              this.service.validateCustomer(email, password).subscribe((clientResponse: any) => {
                const{id,status}=clientResponse;
                console.log(id,status);
                
                if (status) 
                  {
                    localStorage.setItem('id',clientResponse.id);
                    console.log(id);
                  this.service.setUserLoggedIn();
                  this.local.store('firstName',this.firstName);
                  console.log(this.local.retrieve('firstName'));
                  
                  
                  this.toastr.success("Client Logged In Successfully");
                  this.router.navigate(['/home']);
                  this.local.store('customerstatus',true)
                } else
                 {
                  this.toastr.error("Invalid Credentials");
                }
              });
            }
          });
        }
      });
    }
  }

  sendmail(){
    
  } 
}

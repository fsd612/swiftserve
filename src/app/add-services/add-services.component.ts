import { Component, OnInit } from '@angular/core';
import { BookingService } from '../booking.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-add-services',
  templateUrl: './add-services.component.html',
  styleUrl: './add-services.component.css'
})
export class AddServicesComponent implements OnInit {

  companyName:string='';
  email:string='';
  id: any;
  
  constructor(private service:BookingService,private toastr:ToastrService,private http:HttpClient,private router:RouterModule,private local:LocalStorageService){
  
  }

  ngOnInit(): void {
    this.id=localStorage.getItem('id');
    const storedName = this.local.retrieve('name');
  const storedEmail = this.local.retrieve('email');
  
  this.companyName = storedName !== null && storedName !== undefined ? storedName : '';
  this.email = storedEmail !== null && storedEmail !== undefined ? storedEmail : '';

  if(storedName !==null && storedName !== undefined && typeof storedName==='string'){
    this.companyName=JSON.parse(storedName)
  }
  if(storedEmail !==null && storedEmail !== undefined && typeof storedEmail==='string'){
    this.companyName=JSON.parse(storedEmail)
  }
  }

  serviceReg(serviceForm:any){
    if(serviceForm.valid){
      const data = {...serviceForm.value};
      this.service.regServicesBycompId(data,this.id).subscribe((result:any)=>{
        this.toastr.success("Services added Succesfully");
        serviceForm.resetForm();
        console.log(this.serviceReg);

      },
    (error:any)=>{
      this.toastr.warning("services Not registered Succesfully");
    })
  }
}

}
